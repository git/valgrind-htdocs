/* -- sample1.c -- */
#include <stdlib.h>
#include <stdio.h>

void get_mem() {
  char *ptr;
  ptr = (char *) malloc (10);  /* memory not freed */
}

int main(void)
{
  char *ptr1, *ptr2;
  int i;
  ptr1 = (char *) malloc (512);
  ptr2 = (char *) malloc (512);
  ptr2 = ptr1;            /* causes the memory leak of ptr1 */
  free(ptr2);
  free(ptr1);
  for ( i = 0; i < 512; i++ ) {
    get_mem();
  }
}




/* -- sample2.c -- */
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
  char *chptr;
  char *chptr1;
  int i = 1;
  
  chptr = (char *) malloc(512);
  chptr1 = (char *) malloc (512);
  for (i; i <= 513; i++) {
    chptr[i] = '?';           /* error when i = 513 invalid write */
    chptr1[i] = chptr[i];     /* error when i = 513 invalid read and write */
  }

  free(chptr1);
  free(chptr);
}


