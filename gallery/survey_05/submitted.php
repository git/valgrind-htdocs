<?php
/* http://www.stargeek.com/php_scripts.php?script=12 */

$results = '';

/*------ contact details ------*/
$post = $_POST['Q0'];
$results .= $post['q']. "\n";
$results .= $post['name']. "\n";
$results .= $post['email']. "\n";
$results .= $post['country']. "\n\n";

/*------ using valgrind ------*/
$post = $_POST['Q1'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q2'];
$results .= $post['q']. "\n";
$results .= $post['rb']. "\n\n";

$post = $_POST['Q3'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q4'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q5'];
$results .= $post['q']. "\n";
if ( !empty( $post['cb1'] ) )
	$results .= $post['cb1']. "\n";
if ( !empty( $post['cb2'] ) )
	$results .= $post['cb2']. "\n";
if ( !empty( $post['cb3'] ) )
	$results .= $post['cb3']. "\n";
if ( !empty( $post['cb4'] ) )
	$results .= $post['cb4']. "\n";
if ( !empty( $post['cb5'] ) )
		$results .= $post['cb5']. "\n";
if ( !empty( $post['txt'] ) ) {
	$results .= $post['cmt']. "\n";
	$results .= $post['txt']. "\n";
}
$results .= "\n";

$post = $_POST['Q6'];
$results .= $post['q']. "\n";
if ( !empty( $post['txt0'] ) )
	$results .= "memcheck   ". $post['txt0']. "\n";
if ( !empty( $post['txt1'] ) )
	$results .= "addrcheck  ". $post['txt1']. "\n";
if ( !empty( $post['txt2'] ) )
	$results .= "cachegrind ". $post['txt2']. "\n";
if ( !empty( $post['txt3'] ) )
	$results .= "helgrind   ". $post['txt3']. "\n";
if ( !empty( $post['txt4'] ) )
	$results .= "callgrind  ". $post['txt4']. "\n";
if ( !empty( $post['txt5'] ) )
	$results .= "massif     ". $post['txt5']. "\n";
if ( !empty( $post['txt6'] ) )
	$results .= "other      ". $post['txt6']. "\n";
$results .= "\n";

$post = $_POST['Q7'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q8'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

/*------ valgrind on other platforms ------*/
$post = $_POST['Q9'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

/*------ software issues ------*/
$post = $_POST['Q10'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q11'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q12'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q13'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

/*------ non-software issues ------*/
$post = $_POST['Q14'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q15'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

/*------ project details ------*/
$post = $_POST['Q16'];
$txt = ' Project name : 
 Website (if one exists) : 
 Brief description    : 
 Language(s)          : (estimate proportions if > 1, eg. 80% C, 20% Fortran)
 Number of programmers: 
 Size (lines of code) : 
 Number of users      : ';
$txt = ereg_replace( "\n", "", $txt );
$tmp = ereg_replace( "\n", "", $post['txt'] );
$tmp = ereg_replace( "\r", "", $tmp );
if ( strcmp( $tmp, $txt ) != 0 ) {
	$results .= $post['q']. "\n";
	$results .= $post['txt']. "\n\n";
}
$results .= $post['q2']. "\n";
$results .= $post['rb']. "\n\n";

/*------ surveys ------*/
$post = $_POST['Q17'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

$post = $_POST['Q18'];
$results .= $post['q']. "\n";
$results .= $post['txt']. "\n\n";

/*
echo "<pre>\n";
print( $results );
echo '</pre>';
*/

/* send in the results by email. */
$mail_to = "surveys@valgrind.org";
mail( "$mail_to", "Survey Submitted", 
			$results,   "From: www.valgrind.org" );
?>

<h1>Valgrind Survey</h1>

<p>Your survey results have been emailed to 
<?php echo $mail_to; ?>.</p>

<p>Thank you for your time and support in helping to improve
Valgrind.<p>

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
