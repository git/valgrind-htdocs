#!/usr/bin/perl -w
# perl script to remove the superfluous html headings
# from the generated valgrind docs so they fit comfortably
# into the website.

use strict;

# the list of html dirs to use
@main::html_dirs = ( '../docs/valgrind-FAQ',
                     '../docs/vg-bookset' );

# if we have an argument, only go into that dir
my ( $action, $dir_name ) = check_arg();
if ( $action eq 'invalid' ) {
  die "\nQuitting: no such directory: $dir_name\n\n";
}
elsif ( $action eq 'single' ) { 
#  prune_dirs( $dir_name);
  prune_dir( $dir_name );
}
elsif ( $action eq 'all' ) {
  foreach( @main::html_dirs ) {
    prune_dir( $_ );
  }
}



sub prune_dir {
  my $dir = $_[0];
  print "\npruning files in dir '$dir'";

  chdir( $dir ) || die "Cannot chdir to $dir\n";
  # make DIR a local variable in any name space
	local( *DIR );
	opendir( DIR, "." );
  # read file/directory names in that directory into @names 
  my @files = readdir( DIR ) or die "Unable to read current dir:$!\n";
  closedir( DIR );

	my $ext = "html";
	foreach my $file (@files) {
		next if ( $file eq ".");    # skip the current directory entry
		next if ( $file eq "..");   # skip the parent  directory entry
		next if ( -d $file );       # skip any sub-dirs
		if ( $file =~ /\.$ext$/ ) {
		  print "\n  pruning: $file"; 
      # prune this file's header + footer
      prune_file( $file );
	  }
	}
	print "\ndone.\n\n";
}


# write the contents of infile to outfile,
# skipping the stuff we don't want as we go
sub prune_file {
  my $infile; my $outfile;
	$infile  = $_[0];
  $outfile = $infile . '.pruned';

	# open the edited distro file for reading
	open( INFILE, "< $infile" )
	  or die "Cannot open $infile for reading\n";

	my $line = <INFILE>;
	# skip lines till we see '<body '
	while ( $line !~ /<body/i ) {
	  # if this file has already been pruned, stop right here
		if ( eof(INFILE) ) {
			print "  ... previously pruned";
			close( INFILE );
			return;
		}
		$line = <INFILE>;
	}

  # make sure we have the body endtag 
  while ( $line !~ />/ ) {
		$line = <INFILE>;
	}
	# print "  body --> $line";
  # move to next line
  $line = <INFILE>;

  # open the new file for writing
  open( OUTFILE, "> $outfile" )
    or die "Cannot open $outfile for writing\n";

	while( $line !~ /<\/body/i ) {
    print OUTFILE $line;
    $line = <INFILE>;
  }

  # forget everything past the closing body tag
	close( INFILE );
	close( OUTFILE );

  # move .html.pruned to .html
  rename( $outfile, $infile ) 
	  or die "Can't rename $outfile to $infile: $!";
}
  


sub check_arg {
  my $dname;
  my $retval = 'invalid';

  if ( $#ARGV < 0 ) {
    $retval = 'all';
  }
  else {
    my $arg = $ARGV[0];
    $dname = $arg;
    
    # check if this dir is in the html_dirs list
    for ( @main::html_dirs ) {
      if ( $_ eq $arg ) {
        $retval = 'single';
        last;
      }
    }
  }

  ( $retval, $dname );
}


