<?php
/* phpinfo(); */
require_once( '.htconfx' );
include_once( 'funcs.php' );
include( 'menu.php' );
/* the paranoids are after me */
$request_uri = strip_tags( $_SERVER['REQUEST_URI'] );
$curr_page = leafMenu( $request_uri );
/*$curr_page = leafMenu( $_SERVER['REQUEST_URI'] );*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
 <title><?php echo $curr_page['title']; ?></title>
 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
 <meta http-equiv="Content-Style-Type" content="text/css"/>
 <style type="text/css">@import url('/css/valgrind.css');</style>
 <link rel="stylesheet" type="text/css" href="/css/valgrind.css" />
 <link rel="shortcut icon" href="/favicon.ico" />
 <link rel="icon" href="/favicon.ico" type="image/x-icon" />
 <meta name="description" content="Official Home Page for valgrind, a suite of tools for debugging and profiling. Automatically detect memory management and threading bugs, and perform detailed profiling.  The current stable version is <?php echo $config['release-version']; ?>."/>
 <meta name="keywords" content="Valgrind Memcheck Cachegrind Callgrind Massif Helgrind memory-management uninitialised memory memory leaks passing uninitialised memory overlapping pointers misuses of the POSIX pthreads"/>
</head>

<!-- dont we all just wish IE && Netscape 4.x to /dev/null -->
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" 
      bgcolor="#ffffff" text="#202020" 
      link="#000099" alink="#0000ff" vlink="#000099">

<div class="outer-box">
<div class="inner-box">

<?php
if ( $_SERVER['SCRIPT_NAME'] == "/index.html" ) {
?>
<table width="100%" cellspacing="0" cellpadding="12" border="0">
<tr>
 <td valign="top" width="1%">
  <?php homeMenu(); ?>
 </td>
 <td valign="top" width="99%">
  <div id="index-page">
<?php
} else {
?>
<table width="100%" cellspacing="0" cellpadding="12" border="0">
<tr>
 <td width="1%" align="center">
  <a href="/"><?php echo image('st-george_sm.png','121','127','Valgrind Home'); ?></a>
 </td>
 <td width="99%" align="center" valign="middle">
  <span class="topnav">
<?php echo $curr_page['top-nav']; ?>
  </span>
  <div class="subnav">
<?php echo $curr_page['sub-nav']; ?>
  </div>
 </td>
</tr>
<tr>
 <td colspan="2" valign="top" width="100%">
  <div id="leaf-page">
<?php
}
?>
