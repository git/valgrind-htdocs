<?php
if ( isset($included_funcs) ) {
  return;
}
$included_funcs = true;


function image( $img, $w, $h, $alt, $align='' )
{
  $tmp  = '<img src="/images/'. $img .'" width="'.$w.'" height="'.$h.'" alt="' .$alt. '" title="' .$alt. '" border="0"';
  if ( $align != '' )
    $tmp .= ' align="'.$align.'"';
  $tmp .= ' />'."\n";
  return $tmp;
}



/* If 'txt' is empty, uses the default text in $config,
   otherwise, uses the supplied text.  
   Returns a properly formed link. */
function vglink( $arr, $txt='' )
{
  global $config;
  $vg_arr = $config[$arr];     /* get the array */
  $vg_txt = ( $txt == '' ) ? $vg_arr['txt'] : $txt;
  $retval = '<a href="'.$vg_arr['url']. '">' .$vg_txt. "</a>";
  return $retval;
}



/* Takes a name as input, eg. 'julian', and uses this to index
     into the $config['email'] array.  
   The email address is constructed by tacking '@valgrind.org' in
     unicode onto the end of $config['email']['obfusc'].
   If $show_name = true, returned result will use the person's
     real name as the link text; otherwise the email address is used.
   In an effort to obfuscate and baffle those email harvesters,
     all email addresses (both link text and url) are output in
     unicode, eg:
   @valgrind.org --> &#64;&#118;&#97;&#108;&#103;&#114;&#105;&#110;&#100;&#46;&#111;&#114;&#103; */
function vgemail( $who, $show_name=false )
{
  global $config;

  /* index into the $config['email'] array. */
  $vg_arr = $config['email'][$who];

  /* construct the email address first */
  $addr = ( is_array( $vg_arr ) ) ? $vg_arr['obfusc'] : $vg_arr;

  /* tack on '@valgrind.org' */
  $addr .= '&#64;&#118;&#97;&#108;&#103;&#114;&#105;&#110;&#100;&#46;&#111;&#114;&#103;';

  /* now for the link text - use the email address as the default */
  $link_txt = $addr;
  if ( is_array( $vg_arr ) && $show_name ) {
    $link_txt = $vg_arr['name'];
  }

  /* return the result */
  return '<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;' .$addr. '">' .$link_txt. "</a>";
}


/* used by /gallery/survey-xx/ pages */
function chunkNav( $prev, $top, $next )
{
  $prev = ( $prev == '' ) ? '&nbsp;' : '&lt;&lt; '.$prev;
  $next = ( $next == '' ) ? '&nbsp;' : $next. ' &gt;&gt;';
return '
<table class="green" width="100%" cellspacing="0" cellpadding="3" border="0">
 <tr>
  <td valign="middle" align="left" nowrap="nowrap">'.$prev.'</td>
  <th width="80%" align="center">'.$top.'</th>
  <td valign="middle" align="right" nowrap="nowrap">' .$next.'</td>
 </tr>
</table>
';
}


/* small fn to generate an email msg to website admin when 
   a 400 / 401 / 403 / 404 / 500 error is generated. */
function email_error_msg( $err_num, $err_type )
{
  $to      = "website@valgrind.org";
  $from    = "From: website@valgrind.org";
  $subject = "Error " .$err_num. " (" .$err_type. ")";

  $message  = "\n";
  $message .= "Request Date:  " .gmdate("D, d M Y H:i:s") . " GMT\n";
  $message .= "Requested URI: " .$_SERVER['REQUEST_URI']. "\n";
  $message .= "Referer:       " .$_SERVER["HTTP_REFERER"]."\n\n";

  $message .= "Server Name:   " .$_SERVER["SERVER_NAME"]."\n";
  $hostname = gethostbyaddr( $_SERVER["SERVER_ADDR"] );
  $server_info = $hostname. " (" .$_SERVER["SERVER_ADDR"]. ")";
  $message .= "Server Domain: " .$server_info. "\n\n";

  $message .= "User Agent:    " .$_SERVER["HTTP_USER_AGENT"]."\n";
  $hostname = gethostbyaddr( $_SERVER["REMOTE_ADDR"] );
  $message .= "Remote Server: " .$hostname. " (" .$_SERVER["REMOTE_ADDR"]. ")\n";

  mail( $to, $subject, $message, $from,  "-fwebsite@valgrind.org" );
}



/* debugging stuff */
function myPrint( $var )
{
  print "<pre>\n";
  print_r( $var );
  print "</pre>\n";
}

function myLog( $var )
{
  $tmp = "\nArray (";
  $i = 0;
  foreach( $var as $value ) {
    $tmp .= "\n  \$var[$i] => $value";
    $i++;
  }
  $tmp .= "\n)";
  error_log( $tmp );
}

?>
