<?php

$info = array( 
  array( 'url'=>'about.html',     'tag'=>'About' ),
  array( 'url'=>'news.html',      'tag'=>'News' ),
  array( 'url'=>'tools.html',     'tag'=>'Tool Suite' ),
  array( 'url'=>'platforms.html', 'tag'=>'Supported Platforms' ),
  array( 'url'=>'developers.html','tag'=>'The Developers' )
);

$source_code = array(
  array( 'url'=>'current.html',   'tag'=>'Current Releases' ),
  array( 'url'=>'old.html',       'tag'=>'Release Archive' ),
  array( 'url'=>'variants.html',  'tag'=>'Variants / Patches' ),
  array( 'url'=>'repository.html','tag'=>'Code Repository' ),
  array( 'url'=>'guis.html',      'tag'=>'Valkyrie / GUIs' )
);

$docs = array(
  array( 'url'=>'manual/index.html',      'tag'=>'Table of Contents' ),
  array( 'url'=>'manual/QuickStart.html', 'tag'=>'Quick Start' ),
  array( 'url'=>'manual/FAQ.html',        'tag'=>'FAQ' ),
  array( 'url'=>'manual/manual.html',     'tag'=>'User Manual' ),
  array( 'url'=>'download_docs.html',     'tag'=>'Download Manual' ),
  array( 'url'=>'pubs.html',              'tag'=>'Research Papers' ),
  array( 'url'=>'books.html',             'tag'=>'Books' )
);

$contact = array( 
  array( 'url'=>'mailing_lists.html', 'tag'=>'Mailing Lists and IRC' ),
  array( 'url'=>'bug_reports.html',   'tag'=>'Bug Reports' ),
  array( 'url'=>'features.html',      'tag'=>'Feature Requests' ),
  array( 'url'=>'summary.html',       'tag'=>'Contact Summary' ),
  array( 'url'=>'consultants.html',   'tag'=>'Commercial Support' )
);

$help = array(
  array( 'url'=>'contributing.html', 'tag'=>'Contributing' ),
  array( 'url'=>'projects.html',     'tag'=>'Project Suggestions' )
);

$gallery = array(
   array( 'url'=>'users.html',       'tag'=>'Projects / Users' ),
   array( 'url'=>'press_media.html', 'tag'=>'Press / Media' ),
   array( 'url'=>'awards.html',      'tag'=>'Awards' ),
   array( 'url'=>'surveys.html',     'tag'=>'Surveys' ),
   array( 'url'=>'artwork.html',     'tag'=>'Artwork / Clothing' )
);



$vgdirs = array(
'info'      => array( 'url'=>'/info/',      'tag'=>'Information',   'files'=>$info   ),
'downloads' => array( 'url'=>'/downloads/', 'tag'=>'Source Code',   'files'=>$source_code ),
'docs'      => array( 'url'=>'/docs/',      'tag'=>'Documentation', 'files'=>$docs   ),
'support'   => array( 'url'=>'/support/',   'tag'=>'Contact',       'files'=>$contact),
'help'      => array( 'url'=>'/help/',      'tag'=>'How to Help',   'files'=>$help  ),
'gallery'   => array( 'url'=>'/gallery/',   'tag'=>'Gallery',       'files'=>$gallery)
);



/* Called in /php/header.php.
   Generates a left-aligned list of categories + sub-categories.
   Only shown on the Home page */
function homeMenu()
{
  global $vgdirs;

  echo '<div id="menu">'."\n";
  foreach( $vgdirs as $dir ) {
    echo '    <ul>'."\n";
    echo '     <li class="hdr">' .$dir['tag']. '</li>'."\n";
    $files  = $dir['files'];
    for ( $i=0; $i<sizeof($files); $i++ ) {
      echo '     <li><a href="' .$dir['url'].$files[$i]['url']. '">' .$files[$i]['tag']. '</a></li>'."\n";
    }
    echo '    </ul>'."\n";
  }
  echo '   </div>';
}


/* Called in /php/header.php. 
   Takes as input $_SERVER['SCRIPT_NAME'], eg. /info/contact.html
   Returns the current page title, 
    and generates the top + sub nav bars for leaf pages.
   When a top nav bar item is clicked on, 
    the first leaf page in the list is shown by default. */
function leafMenu( $url ) 
{
  //error_log("-------- leafMenu() ---------"); 
  //error_log("url == " . $url );               

  $curr_page = array();
  $curr_page['name'] = '';
  $curr_page['title'] = 'Valgrind';

  /* if we are on the home page, just return the page title */
  if ( $url == '/' || $url == '/index.html' ) {
    $curr_page['title'] .= ' Home';
  } else {

    $curr_page['top-nav'] = '';
		//    $curr_page['sub-nav'] = " | ";
    $curr_page['sub-nav'] = '';
    $elems = explode( "/", $url );
    //myLog( $elems );

    global $vgdirs;
    foreach( $vgdirs as $dir ) {

      /* top nav bar: if this is _not_ the currently selected
         dir, just make a link. */
      if ( $dir['url'] != '/'.$elems[1].'/' ) {
        $top_nav = '   <a class="crumb" href="' .$dir['url']. '">' .$dir['tag']. '</a>';

      } else {
        /* otherwise, show it in bold, and list the leaf pages */
        $top_nav = '   <span class="crumb"><b>' .$dir['tag']. '</b></span>';

        $files = $dir['files'];

        /* if user has clicked on a top-nav link, show the first
           leaf page in the list as the default */
        if ( $elems[2] == '' ) {
          $elems[2] = $files[0]['url'];
          //myLog( $elems );
        }

        for ( $i=0; $i<sizeof($files); $i++ ) {
          /* if this is the requested page, show it in bold in
             the sub nav list, and get the page title */
          if ( $files[$i]['url'] == $elems[2] ) {
            $curr_page['title'] .= ': ' .$files[$i]['tag'];
            $curr_page['name']   = $files[$i]['tag'];
            $curr_page['sub-nav'] .= '<span class="crumb"><b>'.$files[$i]['tag']."</b></span>\n";
          } else {
            /* else, just show it as a link */
            $curr_page['sub-nav'] .= '<a class="crumb" href="'.$dir['url'].$files[$i]['url'].'">' .$files[$i]['tag']. "</a>\n";
          }
          $curr_page['sub-nav'] .= "   | ";
        }
				if ( $curr_page['sub-nav'] != '' ) {
					$curr_page['sub-nav'] = '   | ' .$curr_page['sub-nav']. "\n";
				}

      }
      $curr_page['top-nav'] .= $top_nav. "\n";
    }

  }  /* end else */

  return $curr_page;
}


?>
