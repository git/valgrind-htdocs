# on the real site, this variable is overridden in the environment:
SITE=/home/de/WebSites/valgrind-www/trunk
# (I have to say that that looks a bit odd as a default value)

# the .htaccess is created in place, so don't want to delete
# it, so exclude it .htaccess.in doesn't need to be there,
# so exclude that too
# Also exclude bzip archives in downloads, since they are put in manually

install: $(SITE)/.htaccess
	rsync --delete-after --perms --chmod=D+x,u+rw,go-w,+r --exclude /.htaccess.in --exclude /.htaccess --exclude /downloads/\*\*.bz2 --exclude /downloads/\*\*.diff --recursive --links --times --group ./ $(SITE)  

# the site-dependent .htaccess file:
$(SITE)/.htaccess: .htaccess.in Makefile
	echo "# DO NOT MODIFY THIS FILE: change .htaccess.in instead" > $@ && cat .htaccess.in >> $@ && echo "php_value include_path \""$(SITE)/php"\"" >> $@